## EJERCICIO 17.4

En este ejercicicio construiremos un proograma Python que sirva cualquier invocacion que se le realice con una redireccion a otro recurso, que bien puede ser de si mismo, o externo a partir de una liosta con URLs.

Para implementarlo, utilizaremos los paquetes socket - random y webbrowser. Crearemos un socket que nos permita establecer la conexion cliente-servidor (en nuestro caso lo establecemos como predeterminado la direccion IP de localhost y el puerto 1234) siguiendo la estructura vista en clase.

Como la funcion de este ejercicio es que nuestro programa elija urls aleatorias que se vayan cargando cuando conectamos con el cliente, crearemos una lista donde incluiremos las posibles opciones de urls. Con la funcion *random.choice*, se ira eligiendo de forma aletaoria de esa lista cada vez que recarguemos la pagina del navegador. 

Utilizando la paqueteria webbrowser, usaremos la funcion *webbrowser.open* para que nuestro navegador cuando reciba una de las urls, se abra en otra ventana de nuestro navegador.
