## Ejercicio 17.4:

## Escribiremos un progarama que sirva cualquier invoacion que se le realice con una redireccion a otro recurso aleatorio

import socket
import random
import webbrowser


mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
mySocket.bind(('localhost', 1234))

mySocket.listen(5)

while True:
    print("Waiting for connection...")
    (recvSocket, address) = mySocket.accept()
    print("HTTP request received.")
    received = recvSocket.recv(2048)
    print(received)

    recibido = received.decode()
    peticion = recibido.split(' ')[1]
    print(peticion)

    url_aleatoria = random.choice(['https://www.vogue.es/', 'https://smoda.elpais.com/', 'https://www.ifema.es/mbfw-madrid'])

    response = "HTTP/1.1 200 OK\r\n\r\n" \
                + "Url: " + str(url_aleatoria) + "\r\n\r\n" \


    url = webbrowser.open(url_aleatoria)
    recvSocket.send(response.encode('ascii'))
    recvSocket.close()
